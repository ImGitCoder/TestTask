package ru.gitcoder.task.io;

import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.comphenix.protocol.wrappers.WrappedDataWatcher;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.Optional;

public class ProtocolSupport {

    public void setCustomNameEntity(Entity entity, Player player) {
        WrappedDataWatcher metadata = new WrappedDataWatcher();

        Optional<?> opt = Optional
                .of(WrappedChatComponent
                        .fromChatMessage(player.getName())[0].getHandle());
        metadata.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(2, WrappedDataWatcher.Registry.getChatComponentSerializer(true)), opt);        metadata.setObject(new WrappedDataWatcher.WrappedDataWatcherObject(3, WrappedDataWatcher.Registry.get(Boolean.class)), true); //custom name visible

        WrapperPlayServerEntityMetadata packet = new WrapperPlayServerEntityMetadata();

        packet.setEntityID(entity.getEntityId());
        packet.setMetadata(metadata.getWatchableObjects());

        packet.sendPacket(player);
    }
}
 