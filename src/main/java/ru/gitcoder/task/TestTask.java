package ru.gitcoder.task;

import net.minecraft.server.v1_13_R2.EntityOcelot;
import org.bukkit.plugin.java.JavaPlugin;
import ru.gitcoder.task.entity.DEntityType;
import ru.gitcoder.task.entity.EntityManager;
import ru.gitcoder.task.entity.SimpleEntityManager;
import ru.gitcoder.task.entity.custom.AngryOcelotEntity;
import ru.gitcoder.task.entity.handler.EntitySqlHandler;
import ru.gitcoder.task.entity.handler.SimpleEntitySqlHandler;
import ru.gitcoder.task.io.ProtocolSupport;
import ru.gitcoder.task.listener.TaskListener;
import ru.gitcoder.task.mysql.MySqlConnection;

public class TestTask extends JavaPlugin {

    private final EntityManager entityManager = SimpleEntityManager.create();

    // not getter pls
    private EntitySqlHandler entitySqlHandler;
    // not getter pls
    private MySqlConnection connection;

    private final ProtocolSupport protocolSupport = new ProtocolSupport();

    @Override
    public void onEnable() {
        saveDefaultConfig();

        // Подключаемся к базе данных
        connection = MySqlConnection.newBuilder().user(getConfig().getString("MySQL.User")).host(getConfig().getString("MySQL.Host"))
                .password(getConfig().getString("MySQL.Password")).create();

        // Создаем обработчик запросов в миску
        entitySqlHandler = SimpleEntitySqlHandler.create(connection);

        // Создаём таблицы
        entitySqlHandler.createTable();

        // Регистрируем всех кастомных ентити
        entityManager.registerEntity(DEntityType.OCELOT, EntityOcelot.class, AngryOcelotEntity::new);

        // Регистрируем листенеры
        getServer().getPluginManager().registerEvents(new TaskListener(entityManager, entitySqlHandler, protocolSupport, this), this);
    }
}
