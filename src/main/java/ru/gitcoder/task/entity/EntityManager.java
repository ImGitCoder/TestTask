package ru.gitcoder.task.entity;

import net.minecraft.server.v1_13_R2.Entity;
import net.minecraft.server.v1_13_R2.World;
import org.bukkit.Location;

import java.util.function.Function;

public interface EntityManager {

    org.bukkit.entity.Entity spawnEntity(DEntityType type, Location location);

    void registerEntity(DEntityType type, Class<? extends Entity> classEnt, Function<World, Entity> fun);
}
