package ru.gitcoder.task.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum DEntityType {

    OCELOT(0, "angry_ocelot", "ocelot");

    private final int id;

    private final String customName;
    private final String name;
}