package ru.gitcoder.task.entity.handler;

import java.sql.Timestamp;

public interface EntitySqlHandler {

    void save(String playerName, String ocelotName, Timestamp date);

    void createTable();
}
