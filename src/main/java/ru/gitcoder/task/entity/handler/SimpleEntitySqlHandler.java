package ru.gitcoder.task.entity.handler;

import ru.gitcoder.task.mysql.MySqlConnection;

import java.sql.Timestamp;

public class SimpleEntitySqlHandler implements EntitySqlHandler {

    private final MySqlConnection connection;

    public static SimpleEntitySqlHandler create(MySqlConnection connection) {
        return new SimpleEntitySqlHandler(connection);
    }

    private SimpleEntitySqlHandler(MySqlConnection connection) {
        this.connection = connection;
    }

    @Override
    public void save(String playerName, String entityName, Timestamp date) {
        connection.execute("INSERT INTO `Entities` (`PlayerName`, `EntityName`, `Date`) VALUES (?, ?, ?)",
                playerName, entityName, date);
    }

    @Override
    public void createTable() {
        connection.execute("CREATE TABLE IF NOT EXISTS `Entities` (" +
                "`id` INT(11) NOT NULL AUTO_INCREMENT, " +
                "`PlayerName` VARCHAR(16) NOT NULL, " +
                "`EntityName` VARCHAR(5) NOT NULL, " +
                "`Date` TIMESTAMP" +
                "PRIMARY KEY (`id`)" +
                ") ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;");
    }
}
