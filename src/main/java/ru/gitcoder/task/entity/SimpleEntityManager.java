package ru.gitcoder.task.entity;

import com.mojang.datafixers.DataFixUtils;
import com.mojang.datafixers.types.Type;
import gnu.trove.map.TIntObjectMap;
import gnu.trove.map.hash.TIntObjectHashMap;
import net.minecraft.server.v1_13_R2.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_13_R2.CraftWorld;
import org.bukkit.entity.Entity;

import java.util.Map;
import java.util.function.Function;

public class SimpleEntityManager implements EntityManager {

    private final TIntObjectMap<EntityTypes<?>> entities = new TIntObjectHashMap<>();

    public static EntityManager create() {
        return new SimpleEntityManager();
    }

    private SimpleEntityManager() {
    }

    @Override
    public Entity spawnEntity(DEntityType type, Location location) {
        if (entities.get(type.getId()) == null) {
            return null;
        }

        if (location.getWorld() == null) {
            return null;
        }

        net.minecraft.server.v1_13_R2.Entity entity = entities.get(type.getId()).a(((CraftWorld) location.getWorld()).getHandle(),
                null, null, null, new BlockPosition(location.getBlockX(), location.getBlockY(), location.getBlockZ()), true, false);

        if (entity == null) {
            return null;
        }

        return entity.getBukkitEntity();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void registerEntity(DEntityType type, Class<? extends net.minecraft.server.v1_13_R2.Entity> classEntity, Function<World, net.minecraft.server.v1_13_R2.Entity> fun) {
        Map<Object, Type<?>> registry = (Map<Object, Type<?>>) DataConverterRegistry.a()
                .getSchema(DataFixUtils.makeKey(1628)).findChoiceType(DataConverterTypes.n).types();

        registry.put("minecraft:" + type.getCustomName(), registry.get("minecraft:" + type.getName()));

        entities.put(type.getId(), EntityTypes.a(type.getCustomName(), EntityTypes.a.a(classEntity, fun)));
    }
}
