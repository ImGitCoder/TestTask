package ru.gitcoder.task.listener;

import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_13_R2.entity.CraftEntity;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import ru.gitcoder.task.TestTask;
import ru.gitcoder.task.entity.DEntityType;
import ru.gitcoder.task.entity.EntityManager;
import ru.gitcoder.task.entity.custom.AngryOcelotEntity;
import ru.gitcoder.task.entity.handler.EntitySqlHandler;
import ru.gitcoder.task.io.ProtocolSupport;

import java.sql.Timestamp;

public class TaskListener implements Listener {

    private final EntityManager entityManager;
    private final EntitySqlHandler entitySqlHandler;
    private final ProtocolSupport protocolSupport;

    public TaskListener(EntityManager manager, EntitySqlHandler handler, ProtocolSupport protocolSupport, TestTask task) {
        this.entityManager = manager;
        this.entitySqlHandler = handler;
        this.protocolSupport = protocolSupport;
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        LivingEntity deathEntity = event.getEntity();

        // Killer which dead inside :D
        Player killer = deathEntity.getKiller();

        if (killer == null) {
            return;
        }

        if (deathEntity.getType().equals(EntityType.ZOMBIE)) {
            event.getDrops().clear();

            Entity spawnEntity = entityManager.spawnEntity(DEntityType.OCELOT,
                    event.getEntity().getLocation());

            if (spawnEntity != null) {
                spawnEntity.setCustomName(RandomStringUtils.randomAlphanumeric(5));
                spawnEntity.setCustomNameVisible(true);
                return;
            }
        }

        if (!(((CraftEntity) deathEntity).getHandle() instanceof AngryOcelotEntity)) {
            return;
        }

        // clear drops of entity
        event.getDrops().clear();

        Item item = deathEntity.getWorld().dropItemNaturally(deathEntity.getLocation(),
                new ItemStack(Material.LEATHER));

        for (Player player : Bukkit.getOnlinePlayers()) {
            protocolSupport.setCustomNameEntity(item, player);
        }

        // Сохраняём всё в базу данных
        entitySqlHandler.save(killer.getName(), deathEntity.getCustomName(), new Timestamp(System.currentTimeMillis()));
    }
}
