package ru.gitcoder.task.mysql;

/**
 * Обработчик ответа базы данных
 */
public interface ResponseHandler<H, R> {

    /**
     * Обработка асинхронного ответа БД
     */
    R handleResponse(H handle) throws Exception;

    /**
     * Обработка ошибок БД
     *
     * @param throwable - эксепшн
     */
    default void handleException(Throwable throwable) {
        throwable.printStackTrace();
    }
}
