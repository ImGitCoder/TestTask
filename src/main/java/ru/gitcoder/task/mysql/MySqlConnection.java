package ru.gitcoder.task.mysql;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import lombok.Builder;
import lombok.NonNull;

import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.IntConsumer;

public class MySqlConnection {

    private final ExecutorService executorService = Executors.newCachedThreadPool(
            new ThreadFactoryBuilder().setNameFormat("MySQL-Worker #%s").setDaemon(true).build());

    private final String host;
    private final MysqlDataSource source;

    private Connection connection = null;

    @Builder(builderMethodName = "newBuilder", builderClassName = "MySqlBuilder", buildMethodName = "create")
    private MySqlConnection(@NonNull String host, @NonNull String user, @NonNull String password) {
        this.host = host;

        this.source = new MysqlDataSource();
        this.source.setServerName(host);
        this.source.setPort(3306);
        this.source.setUser(user);
        this.source.setPassword(password);
        this.source.setAutoReconnect(true);
        this.source.setCharacterEncoding("UTF-8");
    }

    private PreparedStatement createStatement(String query, int generatedKeys, Object... objects) throws SQLException {
        PreparedStatement ps = getConnection().prepareStatement(query, generatedKeys);

        if (objects != null) {
            for (int i = 0; i < objects.length; i++) {
                Object object = objects[i];

                if (object == null) {
                    ps.setNull(i + 1, Types.VARCHAR);
                } else {
                    ps.setObject(i + 1, objects[i]);
                }
            }
        }

        if (objects == null || objects.length == 0) {
            ps.clearParameters();
        }

        return ps;
    }

    public void executeQuery(String query, ThrowableConsumer result, Object... objects) {
        executorService.submit(() -> {
            try (PreparedStatement ps = createStatement(query, PreparedStatement.NO_GENERATED_KEYS, objects)) {
                try (ResultSet rs = ps.executeQuery()) {
                    result.accept(rs);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        });
    }

    public void execute(String query, Object... objects) {
        executorService.submit(() -> {
            try (PreparedStatement ps = createStatement(query, PreparedStatement.NO_GENERATED_KEYS, objects)) {
                ps.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void execute(String query, IntConsumer result, Object... objects) {
        executorService.submit(() -> {
            int i = -1;

            try (PreparedStatement ps = createStatement(query, PreparedStatement.RETURN_GENERATED_KEYS, objects)) {
                ps.execute();

                try (ResultSet rs = ps.getGeneratedKeys()) {
                    if (rs.next()) {
                        i = rs.getInt(1);
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            result.accept(i);
        });
    }

    public <T> T executeQuery(String query, ResponseHandler<ResultSet, T> handler, Object... objects) {
        try (PreparedStatement ps = createStatement(query, PreparedStatement.NO_GENERATED_KEYS, objects);
             ResultSet rs = ps.executeQuery()) {

            return handler.handleResponse(rs);
        } catch (Exception e) {
            handler.handleException(e);

            return null;
        }
    }

    public Connection getConnection() {
        refreshConnection();

        return connection;
    }

    protected void refreshConnection() {
        try {
            if (connection != null && !connection.isClosed() && connection.isValid(1000)) {
                return;
            }

            this.connection = source.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Error with connection on MySql - " + this.host, e);
        }
    }

    public void close() {
        try {
            this.connection.close();
        } catch (SQLException ignored) {
        }
    }

    public interface ThrowableConsumer {

        void accept(ResultSet rs) throws Exception;

    }
}
